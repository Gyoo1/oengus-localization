# Oengus localization files

This is a repository to store and update localization files for [Oengus](https://oengus.io)

Localization files are JSON files (one per language) structured per location in the application.

To update existing files, clone the repository and make a pull request everytime you make an update.

For any new language, contact Gyoo on the [Oengus Discord server](https://discord.gg/ZZFS8YT) first

# How to translate

There are 2 ways to translate :
- Either you're comfortable editing the JSON file directly, then go for it, it works,
- Or you want something a bit more user-friendly, then I recommend using [i18n-editor](https://github.com/jcbvm/i18n-editor) for the task. After opening it, go to `File > Import Project...` and select the folder that contains translations. A settings file is provided in the folder so it should open nicely.
